﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionScript : MonoBehaviour {
    
    public bool bubbleHitWall;
    public bool bubbleHitBubble;

    public float bubblePositionX;
    public float bubblePositionY;

    public bool IsExploding = false;
    public int CurrentlyCollidingSameColoredBalls = 0;
    public List<Transform> AllSameColoredBallsImCollidingWith = new List<Transform>();
    public int TotalBalls;
    public List<Transform> TotalNumberofBalls;

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 1.55f);
    }

    private void Awake()
    {
        if (Time.time < 1)
        {
            Material mat = GameObject.Find("CanonParent").GetComponent<CannonControl>().mat[Random.Range(0, 4)];
            GetComponent<Renderer>().material = mat;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (!GetComponent<MoveForward>().enabled)
            return;
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (col.transform.tag == "Bubble")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Debug.Log("The ball hit another ball. ");
            bubbleHitBubble = true;
        }

        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (col.gameObject.tag == "Wall")
        {
            //If the GameObject has the same tag as specified, output this message in the console
            Debug.Log("The ball hit the wall");
            bubbleHitWall = true;
        }

        GetComponent<AudioSource>().Play();

        if (col.transform.tag == "Wall" || col.transform.tag == "Bubble")
        {
            if (col.transform.name == "Roof" || col.transform.tag == "Bubble")
            {
                GetComponent<MoveForward>().enabled = false;
                GetComponent<Rigidbody>().isKinematic = true;
            }
            else
            {
                GetComponent<MoveForward>().dir.x = -GetComponent<MoveForward>().dir.x;
            }
            GetComponent<AudioSource>().Play();
        }

        LookForConnected(transform.position);
    }

    void LookForConnected(Vector3 pos)
    {
        Collider[] cols = Physics.OverlapSphere(pos, 1.55f);

        for (int i = 0; i < cols.Length; i++)
        {
            if (!AllSameColoredBallsImCollidingWith.Contains(cols[i].transform) && cols[i].tag == "Bubble" && cols[i].transform != transform) //Check if we hit another bubble, and Make sure we don't collide with our own object
            {
                if (GetComponent<Renderer>().material.color == cols[i].GetComponent<Renderer>().material.color)
                {
                    AllSameColoredBallsImCollidingWith.Add(cols[i].transform);
                    LookForConnectedLoop(cols[i].transform.position);
                    CurrentlyCollidingSameColoredBalls++;
                }
            }
        }
        StartCoroutine("FinishLooking");
    }

    void LookForConnectedLoop(Vector3 pos)
    {
        LookForConnected(pos);
    }

    IEnumerator FinishLooking()
    {
        yield return new WaitForSeconds(0.1f);
        if (CurrentlyCollidingSameColoredBalls > 1)
        {
            for (int i = 0; i < AllSameColoredBallsImCollidingWith.Count; i++)
            {
                AllSameColoredBallsImCollidingWith[i].GetComponent<CollisionScript>().Explode();
                Explode();
            }
        }
        else
        {
            CurrentlyCollidingSameColoredBalls = 0;
        }
    }

    public void Explode()
    {
        Debug.Log("Time to explode!");
        Transform ps = GameObject.Find("ExplosionPS").transform;
        
        //ps.transform.position = transform.position;

        GameObject.Find("Main Game Manager").GetComponent<MainManager>().TotalScore++;
        var emitParams = new ParticleSystem.EmitParams
        {
            startColor = GetComponent<Renderer>().material.color,
            position = transform.position
        };

        ps.GetComponent<ParticleSystem>().Emit(emitParams, 3);
        ps.GetComponent<AudioSource>().Play();

        //Tell my friends to explode
        IsExploding = true;

        //Explode myself

        Destroy(gameObject);

    }
}
