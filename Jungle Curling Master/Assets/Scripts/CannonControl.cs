﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonControl : MonoBehaviour {

    public Material[] mat;

    public float RotateSpeed;
    public float ShootForce;

    public Transform BallPrefab;
    public Transform SpawnPoint;
    public LineRenderer lineRenderer;
    public Slider meter;

    public Renderer BaseRenderer;

    float launchPower;

    // Use this for initialization
    void Start() {
        RotateSpeed = 2;
        ShootForce = 5; // changed from 4 to 5 to debug 
     //   ChangeColor();
    }

    // Update is called once per frame
    void Update() {

        lineRenderer.SetPosition(0, SpawnPoint.position);

        RaycastHit hit;

        if (Physics.Raycast(SpawnPoint.position, GameObject.Find("Line Renderer").transform.forward, out hit))
        {
            lineRenderer.SetPosition(1, hit.point);
        }

        transform.Rotate(-Vector3.forward * Input.GetAxis("Mouse X") * 2f);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (Input.GetKey(KeyCode.RightArrow)){

			transform.Rotate (new Vector3 (0,0,-RotateSpeed));
		}
		if(Input.GetKey(KeyCode.LeftArrow)){

			transform.Rotate (new Vector3 (0,0,RotateSpeed));

		}
		if (Input.GetKeyUp (KeyCode.Space) || Input.GetMouseButtonUp(0)) {

			Transform NewBall = Instantiate (BallPrefab, SpawnPoint.position, SpawnPoint.rotation);
            NewBall.GetComponent<Renderer>().material = BaseRenderer.material;

			NewBall.GetComponent<Rigidbody> ().AddRelativeForce (new Vector3 (ShootForce,ShootForce,0));
            

            Vector3 DirectionVector = SpawnPoint.position - transform.position;
            DirectionVector = DirectionVector * ShootForce;
            NewBall.GetComponent<Rigidbody>().AddForce(DirectionVector);
            NewBall.GetComponent<MoveForward>().ballSpeed = NewBall.GetComponent<MoveForward>().ballSpeed * launchPower * 0.7f;



            ChangeColor();
            GetComponent<AudioSource>().Play();
        }

        if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            launchPower = 0;
        }

        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
        {
            if (launchPower < 1)
                launchPower += Time.deltaTime * 1.3f;
            else
                launchPower = 0;
            
        }
        meter.value = launchPower;
    }
    
    void ChangeColor()
    {
        int chosenMaterial = Random.Range(0, mat.Length);
        BaseRenderer.material = mat[chosenMaterial];
    }
}
