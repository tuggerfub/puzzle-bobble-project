﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour {

    public Material BubbleMaterial { get; set; } //current color
    public Material OriginalBubbleMaterial { get; private set; } //cache the original color
    public GameObject GameObject;
    public Bubble(GameObject gameObject, Material material)


    {
        GameObject = gameObject;
        OriginalBubbleMaterial = BubbleMaterial = material;
    }

    public void Bounce()
    {

    }
    // Use this for initialization
    void Start () {

		
	}



	// Update is called once per frame
	void Update () {
		
	}
}
