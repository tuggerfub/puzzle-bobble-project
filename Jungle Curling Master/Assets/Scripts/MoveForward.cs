﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour {


    public float ballSpeed;
    public Vector3 dir;
    
    // Use this for initialization
    void Start () {
        dir = GameObject.Find("Line Renderer").transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
        
        
        transform.Translate (dir * ballSpeed * Time.deltaTime, Space.World);

        if (ballSpeed > 0)
            ballSpeed -= Time.deltaTime;
        else
        {
            ballSpeed = 0;
        }

        /*

        Rigidbody myRigidbody;
        float radius;
        Vector3 previousPosition;


        myRigidbody = gameObject.GetComponent<Rigidbody>();
        radius = gameObject.GetComponent<SphereCollider>().radius * transform.localScale.x;
        myRigidbody.AddForce(transform.forward + ballSpeed);
        previousPosition = transform.position;
        
         */

        //if gameObject.position > 20 {

        //}

    }




}

   
